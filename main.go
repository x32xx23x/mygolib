package myGoLib

import (
	"fmt"
	"math/big"
)

var one = big.NewInt(1)

func Test() {
	fmt.Println("vim-go")
	return
}

//Finds n,m such that an+bm=1
func BezoutId(a, b *big.Int) (*big.Int, *big.Int) {
	x := ModInverse(a, b)
	y := Div(Sub(one, Mul(a, x)), b)
	return x, y

}

func Int2Str(a *big.Int) string {
	out := ""
	for _, t := range a.Bytes() {
		out = out + string(t)
	}
	return out
}

func Mod(a, b *big.Int) *big.Int {
	return new(big.Int).Mod(a, b)
}
func Mul(a, b *big.Int) *big.Int {
	return new(big.Int).Mul(a, b)
}
func Sub(a, b *big.Int) *big.Int {
	return new(big.Int).Sub(a, b)
}
func Div(a, b *big.Int) *big.Int {
	return new(big.Int).Div(a, b)
}
func ModInverse(a, b *big.Int) *big.Int {
	return new(big.Int).ModInverse(a, b)
}

func Exp(a, b, c *big.Int) *big.Int {
	return new(big.Int).Exp(a, b, c)
}
